import time
import selenium
import pytest
from pages.arena.add_project_page import AddProjectPage
from pages.arena.arena_home_page import ArenaHomePage
from pages.arena.arena_login import ArenaLoginPage
from pages.arena.arena_search_project import SearchProject
from selenium.webdriver import Chrome
from selenium.webdriver.common.by import By
from utils import generate_random_text
from webdriver_manager.chrome import ChromeDriverManager


@pytest.fixture()
def browser():
    driver = Chrome(executable_path=ChromeDriverManager().install())
    driver.get('http://demo.testarena.pl/zaloguj')
    arena_login_page = ArenaLoginPage(driver)
    arena_login_page.login('administrator@testarena.pl', 'sumXQQ72$L')
    yield driver
    time.sleep(1)
    driver.quit()


def test_new_project(browser):
    # przejście do sekcji Projekty
    arena_home_page = ArenaHomePage(browser)
    arena_home_page.click_tools_icon()
    # potwierdzenie że znaleźliśmy się sekcji "Projekty"
    arena_project_page = AddProjectPage(browser)
    arena_project_page.verify_title('Projekty')

    # dodanie projektu
    arena_home_page = AddProjectPage(browser)
    arena_home_page.click_add_project()
    assert browser.current_url == 'http://demo.testarena.pl/administration/add_project'

    # uzupełnienie nazwy projektu i zapisanie go
    arena_home_page = AddProjectPage(browser)
    random_text = generate_random_text(10)
    arena_home_page.wait_for_text_area_load()
    arena_home_page.insert_random_name(random_text)
    arena_home_page.click_save_project()

    # potwierdzenie sukcesu zapisania nowego projektu
    success_message_element = browser.find_element(By.CSS_SELECTOR, "#j_info_box")
    assert success_message_element.is_displayed() is True

    # wyszukiwanie nowego projektu
    search_project = SearchProject(browser)
    search_project.go_to_projects_section()
    search_project.search_for_project(random_text)

    # potwierdzenie znalezienia nowego projektu i wyświetlenie jego nazwy
    project_name = search_project.get_project_name()
    print(f"Found new project with name: {project_name}")
