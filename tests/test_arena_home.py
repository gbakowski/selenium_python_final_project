import time

import pytest
from selenium.webdriver import Chrome
from selenium.webdriver import Firefox
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait
from webdriver_manager.chrome import ChromeDriverManager
from webdriver_manager.firefox import GeckoDriverManager

@pytest.fixture()
def browser():
    driver = Chrome(executable_path=ChromeDriverManager().install())
    driver.get('http://demo.testarena.pl/zaloguj')
    driver.find_element(By.ID, 'email').send_keys('administrator@testarena.pl')
    driver.find_element(By.ID, 'password').send_keys('sumXQQ72$L')
    driver.find_element(By.ID, 'login').click()
    yield driver
    time.sleep(1)
    driver.quit()

# Test - uruchomienie Chroma
def test_should_display_email_in_user_section(browser):
    # Asercja - weryfikacja że logowanie sie faktycznie udała
    user_info = browser.find_element(By.CSS_SELECTOR, '.user-info small')
    assert user_info.text != '' # ta asercja sprawdza czy w oknie logowania nie ma pustego miejsca

    # Weryfikacja czy tytuł otwartej strony zawiera w sobie 'TestArena'
    assert 'TestArena' in browser.title

def test_should_successfully_logout(browser):
    browser.find_element(By.CSS_SELECTOR, '.icons-switch').click()
    assert browser.current_url == 'http://demo.testarena.pl/zaloguj'

def test_should_open_messages_and_display_text_area(browser):
    browser.find_element(By.CSS_SELECTOR, '.icon_mail').click()
    wait = WebDriverWait(browser, 10)  # czekamy max. 10 sekund aby strona załadowała treść
    wait.until(expected_conditions.element_to_be_clickable((By.CSS_SELECTOR, '#j_msgContent')))
    assert browser.current_url == 'http://demo.testarena.pl/moje_wiadomosci'

def test_should_open_projects_page(browser):
    browser.find_element(By.CSS_SELECTOR, '.icon_tools').click()
    assert browser.find_element(By.CSS_SELECTOR, '.content_title').text == 'Projektytest_arena_home.py'


# Test - uruchomienie Firefoxa
def test_my_first_firefox_selenium_test():
    # Uruchomienie przeglądarki Firefox. Ścieżka do geckodrivera (drivera dla Firefoxa)
    # ustawiana automatycznie przez bibliotekę webdriver-manager
    browser = Firefox(executable_path=GeckoDriverManager().install())

    # Otwarcie strony www.google.pl

    # Weryfikacja tytułu

    # Zamknięcie przeglądarki
    browser.quit()