import pytest
import time
from selenium.webdriver import Chrome
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC, expected_conditions

from pages.home import HomePage
from pages.search_result import SearchResultPage


@pytest.fixture()
def browser():
    # Uruchomienie przeglądarki Chrome. Ścieżka do chromedrivera
    # ustawiana automatycznie przez bibliotekę webdriver-manager
    # 1 - To wykona się PRZED każdym testem który korzysta z fixture
    browser = Chrome(executable_path=ChromeDriverManager().install())
    # Otwarcie strony
    browser.get('https://awesome-testing.blogspot.com')
    # 2 - To jes to co zwracamy (przekazujemy) do testów.
    yield browser  # yield = zwróć (granica, czyli co wykona sie przed yield i co wykona sie po yiled)
    # Zamknięcie przeglądarki
    # 3 - To wykona się PO każdym tecie który korzysta z fixture
    browser.quit()


def test_post_count(
        browser):  # - tutaj używamy fixture bo w () wpisujemy "browser" który jest funkcją fixture - patrz powyżej
    # Uruchomienie przeglądarki Chrome. Ścieżka do chromedrivera
    # ustawiana automatycznie przez bibliotekę webdriver-manager
    # browser = Chrome(executable_path=ChromeDriverManager().install())

    # Otwarcie strony
    # browser.get('https://awesome-testing.blogspot.com')
    home_page = HomePage(browser)
    home_page.verify_post_count(4)
    # Pobranie listy tytułów
    # titles = browser.find_elements(By.CSS_SELECTOR, '.post-title')
    #
    # # Asercja że lista ma 4 elementy
    # assert len(titles) == 4

    # Zamknięcie przeglądarki
    # browser.quit()

def test_post_count_after_search(browser):
    home_page = HomePage(browser)
    home_page.search_for('selenium')
    # Uruchomienie przeglądarki Chrome. Ścieżka do chromedrivera
    # ustawiana automatycznie przez bibliotekę webdriver-manager
    # browser = Chrome(executable_path=ChromeDriverManager().install())
    #
    # # Otwarcie strony
    # browser.get('https://awesome-testing.blogspot.com')
    #
    # # Inicjalizacja searchbara i przycisku search
    # search_bar = browser.find_element(By.CSS_SELECTOR, '.gsc-input-input')
    # search_button = browser.find_element(By.CSS_SELECTOR, 'input.gsc-search-button')
    #
    # # Szukanie
    # search_bar.send_keys('selenium')
    # search_button.click()

    # Czekanie na stronę
    search_result_page = SearchResultPage(browser)
    search_result_page.wait_for_load()
    search_result_page.verify_post_count(20)

    # wait = WebDriverWait(browser, 10) # czekamy max. 10 sekund aby strona załadowała treść
    # element_to_wait_for = (By.CSS_SELECTOR, '.status_msg_body') #- status_msg_body to pasek na który czekam, który pojawia się po pierwszej próbie wyszukania w oknie wyszukiwarki
    # wait.until(expected_conditions.visibility_of_element_located(element_to_wait_for))

    # wait.until(lambda browser: len(browser.find_elements(By.CSS_SELECTOR, '.post-title')) == 15) # ta funkcja szuka na stronie do "X" wartości i jeśli ją osiągnie to zalicza test.
    # lambda  - czyli funkcja mirko/anonimowa, której nie widać a która ma 1 parametr który zwraca: return len(browser.find_elements(By.CSS_SELECTOR, '.post-title')) == 15

    # Pobranie listy tytułów
    # titles = browser.find_elements(By.CSS_SELECTOR, '.post-title')
    #
    # # Asercja że lista ma 3 elementy / teraz Asertujemy (sprawdzamy) ile wyników jest po wyszukaniu "selenium" w wyszukiware (jest ich 20)
    # assert len(titles) == 20

    # Zamknięcie przeglądarki
    browser.quit()


def test_post_count_on_2016_label():
    # Uruchomienie przeglądarki Chrome. Ścieżka do chromedrivera
    # ustawiana automatycznie przez bibliotekę webdriver-manager
    #browser = Chrome(executable_path=ChromeDriverManager().install())

    # Otwarcie strony
    # browser.get('https://awesome-testing.blogspot.com')

    # Inicjalizacja elementu z labelką 2016
    home_page = HomePage(browser)
    home_page.click_label('2016')
    search_result_page = SearchResultPage(browser)
    search_result_page.verify_post_count(24)

    # label_2016 = browser.find_element(By.LINK_TEXT, '2016')

    # Kliknięcie na labelkę 2016
    # label_2016.click()

    # Pobranie listy tytułów
    # titles = browser.find_elements(By.CSS_SELECTOR, '.post-title')

    # Asercja że lista ma 1 element
    # assert len(titles) == 24
    # Zamknięcie przeglądarki
    browser.quit()
