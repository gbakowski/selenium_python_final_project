import time

import pytest
from selenium.webdriver import Chrome
from selenium.webdriver import Firefox
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait
from webdriver_manager.chrome import ChromeDriverManager
from webdriver_manager.firefox import GeckoDriverManager

from pages import arena
from pages.arena.arena_home_page import ArenaHomePage
from pages.arena.arena_login import ArenaLoginPage
from pages.arena.arena_messages import ArenaMessagesPage
from pages.arena.add_project_page import ArenaProjectPage
from utils import generate_random_text


@pytest.fixture()
def browser():
    driver = Chrome(executable_path=ChromeDriverManager().install())
    driver.get('http://demo.testarena.pl/zaloguj')
    arena_login_page = ArenaLoginPage(driver)
    arena_login_page.login('administrator@testarena.pl', 'sumXQQ72$L')

    # driver.find_element(By.ID, 'email').send_keys('administrator@testarena.pl')
    # driver.find_element(By.ID, 'password').send_keys('sumXQQ72$L')
    # driver.find_element(By.ID, 'login').click()
    yield driver
    time.sleep(1)
    driver.quit()

# Test - uruchomienie Chroma
def test_should_display_email_in_user_section(browser):
    arena_home_page = ArenaHomePage(browser)
    arena_home_page.verify_displayed_email('administrator@testarena.pl')
    # Asercja - weryfikacja że logowanie sie faktycznie udała
    # user_info = browser.find_element(By.CSS_SELECTOR, '.user-info small')
    # assert user_info.text != ''  # ta asercja sprawdza czy w oknie logowania nie ma pustego miejsca

    # Weryfikacja czy tytuł otwartej strony zawiera w sobie 'TestArena'
    assert 'TestArena' in browser.title

def test_should_successfully_logout(browser):
    arena_home_page = ArenaHomePage(browser)
    arena_home_page.click_logout()
    # browser.find_element(By.CSS_SELECTOR, '.icons-switch').click()
    assert browser.current_url == 'http://demo.testarena.pl/zaloguj'

def test_should_open_messages_and_display_text_area(browser):
    arena_home_page = ArenaHomePage(browser)
    arena_home_page.click_mail()
    arena_messages_page = ArenaMessagesPage(browser)
    arena_messages_page.wait_for_text_area_load()
    # browser.find_element(By.CSS_SELECTOR, '.icon_mail').click()
    # wait = WebDriverWait(browser, 10)  # czekamy max. 10 sekund aby strona załadowała treść
    # wait.until(expected_conditions.element_to_be_clickable((By.CSS_SELECTOR, '#j_msgContent')))
    assert browser.current_url == 'http://demo.testarena.pl/moje_wiadomosci'

def test_should_open_projects_page(browser):
    arena_home_page = ArenaHomePage(browser)
    arena_home_page.click_tools_icon()
    # browser.find_element(By.CSS_SELECTOR, '.icon_tools').click()

    arena_project_page = ArenaProjectPage(browser)
    arena_project_page.verify_title('Projekty')
    # assert browser.find_element(By.CSS_SELECTOR, '.content_title').text == 'Projekty'

# def test_should_add_new_message(browser):
#     random_text = generate_random_text(10)
#     arena_messages_page = ArenaMessagesPage(browser)
#     arena_messages_page.wait_for_text_area_load()
#     arena_messages_page.insert_message(random_text)
#     time.sleep(2)


# Test - uruchomienie Firefoxa
def test_my_first_firefox_selenium_test():
    # Uruchomienie przeglądarki Firefox. Ścieżka do geckodrivera (drivera dla Firefoxa)
    # ustawiana automatycznie przez bibliotekę webdriver-manager
    browser = Firefox(executable_path=GeckoDriverManager().install())

    # Otwarcie strony www.google.pl

    # Weryfikacja tytułu

    # Zamknięcie przeglądarki
    browser.quit()


def test_should_open_duckduckgo_com():
    browser = Chrome(executable_path=ChromeDriverManager().install())

    # Otwarcie strony testareny - pierwsze użycie Selenium API
    browser.get('https://duckduckgo.com')  # funkcja "get" otwiera stronę.

    # Sprawiamy że maksymalizujemy okno przeglądarki
    browser.set_window_size(1024, 768)

    browser.find_element(By.CSS_SELECTOR, 'div h1')  # Tu podajemy CSS Selektor taki jak szukalismy w poniedziałek

    # Weryfikacja czy tytuł otwartej strony zawiera w sobie 'TestArena'
    assert 'DuckDuckGo' in browser.title

    # Zamknięcie przeglądarki
    time.sleep(3)
    browser.quit()
