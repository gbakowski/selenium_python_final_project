from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait


class AddProjectPage:
    def __init__(self, browser):
        self.browser = browser

    def verify_title(self, title):
        assert self.browser.find_element(By.CSS_SELECTOR, '.content_title').text == title

    def click_add_project(self):
        self.browser.find_element(By.XPATH, '//a[text()="Dodaj projekt"]').click()

    def wait_for_text_area_load(self):
        wait = WebDriverWait(self.browser, 10)
        text_area = (By.CSS_SELECTOR, '#name')
        text_area = (By.CSS_SELECTOR, '#prefix')
        text_area = (By.CSS_SELECTOR, '#description')
        wait.until(expected_conditions.element_to_be_clickable(text_area))

    def insert_random_name(self, random_text):
        self.browser.find_element(By.CSS_SELECTOR, '#name').send_keys(random_text)
        self.browser.find_element(By.CSS_SELECTOR, '#prefix').send_keys(random_text)
        self.browser.find_element(By.CSS_SELECTOR, '#description').send_keys(random_text)

    def click_save_project(self):
        self.browser.find_element(By.CSS_SELECTOR, '#save').click()
