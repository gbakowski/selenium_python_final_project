import time

from selenium.webdriver import Keys
from selenium.webdriver.common.by import By


def confirm_search_new_project(random_text, project):
    search_new_project = project.text
    assert random_text in search_new_project
    # self.browser.find_element(By.CSS_SELECTOR, '#j_searchButton').click()


class SearchProject:

    def __init__(self, browser):
        self.browser = browser

    def go_to_projects_section(self):
        self.browser.find_element(By.CSS_SELECTOR, '.activeMenu').click()

    def get_project_name(self):
        project_name_element = self.browser.find_element(By.TAG_NAME, 'td')
        return project_name_element.text

    def search_for_project(self, random_text):
        self.browser.find_element(By.ID, "search").send_keys(random_text)