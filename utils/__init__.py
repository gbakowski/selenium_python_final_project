import random
import string

def generate_random_text(length: object) -> object:   # ta funkcja dodaje losowy teskt w wybranym przez nas selektorze
    result_str = ''.join(random.choice(string.ascii_letters + string.digits) for i in range(length))
    return result_str
